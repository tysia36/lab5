package lab6.l6.lab6.src.domain;


public class Permission {
    private String name;

    public String getName() {
        return name;
    }

    public Permission setName(String name) {
        this.name = name;
        return this;
    }
    
    @Override
    public String toString() {
         return this.getName();
    }
}
