package lab6.l6.lab6.src.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;



import lab6.l6.lab6.src.domain.*;
import lab6.l6.lab6.src.service.*;

public class UserSeviceTest {

	@Test
	public void testFindUsersWhoHaveMoreThanOneAddress() {
		
		Person person1 = new Person();
		Person person2 = new Person();
		
		User usr = new User("name", "Password", person1);
		User UserWith2Address = new User("name2", "haslo", person2);
		
		Address address1 = new Address("Poland", "Koszalin", "Gierczak", 127);
		Address address2 = new Address("Poland", "Cewlino", "Cewlino", 15);
		
		person1.setAddresses(Arrays.asList(address1));
		person2.setAddresses(Arrays.asList(address1,address2));
		
		usr.setPersonDetails(person1);
		UserWith2Address.setPersonDetails(person2);
		
		List<User> users = new ArrayList<User>();
		users.add(usr);
		users.add(UserWith2Address);

		List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);

		assertSame(result.get(0), UserWith2Address); 
		
	}

	@Test
	public void testFindOldestPerson() {
		List<User> users;

		Person osoba1 = new Person();
		Person osoba2 = new Person();
		Person osoba3 = new Person();
		osoba1.setAge(25);
		osoba1.setName("Aniela");
		osoba1.setSurname("��cka");
		
		osoba2.setAge(15);
		osoba2.setName("Anna");
		osoba2.setSurname("Romowa");
		
		
		osoba3.setAge(57);
		osoba3.setName("Andrzej");
		osoba3.setSurname("Nowak");
		
		User user1 = new User("alacka", "P@ssw0rd123", osoba1);
		User user2 = new User("aromowa", "P@ssw0rd321", osoba2);
		User user3 = new User("anowak", "P@ssw0rd098", osoba3);

		users = Arrays.asList(user1, user2, user3);
		
		Person result = UserService.findOldestPerson(users);
		assertSame(82, result.getAge());
		
	}

	@Test
	public void testFindUserWithLongestUsername() {
		List<User> users;

		Person osoba1 = new Person();
		Person osoba2 = new Person();
		Person osoba3 = new Person();
		osoba1.setAge(25);
		osoba1.setName("Aniela");
		osoba1.setSurname("��cka");
		
		osoba2.setAge(15);
		osoba2.setName("Anna");
		osoba2.setSurname("Romowa");
		
		
		osoba3.setAge(57);
		osoba3.setName("Andrzej");
		osoba3.setSurname("Nowak");
		
		User user1 = new User("alacka", "P@ssw0rd123", osoba1);
		User user2 = new User("aromowa", "P@ssw0rd321", osoba2);
		User user3 = new User("anowak", "P@ssw0rd098", osoba3);

		users = Arrays.asList(user1, user2, user3);
		User result = UserService.findUserWithLongestUsername(users);
		assertSame("saviola", result.getName());
	}

	@Test
	public void testGetNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() {
		
		List<User> users;
		

		Person osoba1 = new Person();
		Person osoba2 = new Person();
		Person osoba3 = new Person();
		osoba1.setAge(25);
		osoba1.setName("Aniela");
		osoba1.setSurname("��cka");
		
		osoba2.setAge(15);
		osoba2.setName("Anna");
		osoba2.setSurname("Romowa");
		
		
		osoba3.setAge(57);
		osoba3.setName("Andrzej");
		osoba3.setSurname("Nowak");
		
		User user1 = new User("alacka", "P@ssw0rd123", osoba1);
		User user2 = new User("aromowa", "P@ssw0rd321", osoba2);
		User user3 = new User("anowak", "P@ssw0rd098", osoba3);

		users = Arrays.asList(user1, user2, user3);
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		
		assertEquals("Kamil Kubicki,Artur Wozniak,Janusz Sawada", result);
	}
	

	@Test
	public void testGetSortedPermissionsOfUsersWithNameStartingWithA() {
		
		Person osoba1 = new Person();
		Person osoba2 = new Person();
		Person osoba3 = new Person();
		osoba1.setAge(25);
		osoba1.setName("Aniela");
		osoba1.setSurname("��cka");
		
		osoba2.setAge(15);
		osoba2.setName("Anna");
		osoba2.setSurname("Romowa");
		
		
		osoba3.setAge(57);
		osoba3.setName("Andrzej");
		osoba3.setSurname("Nowak");
		
		User user1 = new User("alacka", "P@ssw0rd123", osoba1);
		User user2 = new User("aromowa", "P@ssw0rd321", osoba2);
		User user3 = new User("anowak", "P@ssw0rd098", osoba3);
		List<User> users = Arrays.asList(user1, user2,user3);
		
		Role roleUser = new Role();
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		osoba1.setRole(roleUser);
		osoba2.setRole(roleGuest);
		osoba3.setRole(roleAdmin);
		
		Permission permissionE = new Permission();
		Permission permissionW = new Permission();
		Permission permissionR = new Permission();
		
		permissionE.setName("execute");
		permissionW.setName("write");
		permissionR.setName("read");
		
		List<Permission> permGuest = Arrays.asList(permissionR);
		List<Permission> permAdmin = Arrays.asList(permissionE,permissionR,permissionW);
		
		roleGuest.setPermissions(permGuest);
		roleAdmin.setPermissions(permAdmin);
		
		assertEquals(Arrays.asList("read"),UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users));
		
	}

	@Test
	public void testGroupUsersByRole() {
		List<User> users;
		
		Person osoba1 = new Person();
		Person osoba2 = new Person();
		Person osoba3 = new Person();
		
		
		osoba1.setAge(25);
		osoba1.setName("Aniela");
		osoba1.setSurname("��cka");
		
		osoba2.setAge(15);
		osoba2.setName("Anna");
		osoba2.setSurname("Romowa");
		
		
		osoba3.setAge(57);
		osoba3.setName("Andrzej");
		osoba3.setSurname("Nowak");
		
		User user1 = new User("alacka", "P@ssw0rd123", osoba1);
		User user2 = new User("aromowa", "P@ssw0rd321", osoba2);
		User user3 = new User("anowak", "P@ssw0rd098", osoba3);
		
		List<User> guestList = Arrays.asList(user3);
		List<User> adminList = Arrays.asList(user1);
		users = Arrays.asList(user1, user2);
		
		Role roleUser = new Role();
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		osoba1.setRole(roleUser);
		osoba2.setRole(roleGuest);
		osoba3.setRole(roleAdmin);
		
		Map<Role, List<User>> result = UserService.groupUsersByRole(users);
		Map<Role, List<User>> test = new HashMap<Role, List<User>>();
		test.put(roleGuest, guestList );
		test.put(roleAdmin, adminList);
		assertEquals(test,result);
	}
	

	@Test
	public void testPartitionUserByUnderAndOver18() {
		List<User> users;
		
		Person osoba1 = new Person();
		Person osoba2 = new Person();
		Person osoba3 = new Person();


		osoba1.setAge(25);
		osoba1.setName("Aniela");
		osoba1.setSurname("��cka");

		osoba2.setAge(15);
		osoba2.setName("Anna");
		osoba2.setSurname("Romowa");


		osoba3.setAge(57);
		osoba3.setName("Andrzej");
		osoba3.setSurname("Nowak");
		User user1 = new User("alacka", "P@ssw0rd123", osoba1);
		User user2 = new User("aromowa", "P@ssw0rd321", osoba2);
		User user3 = new User("anowak", "P@ssw0rd098", osoba3);
		List<User> under18List = Arrays.asList(user1);
		List<User> over18List = Arrays.asList(user2);
		users = Arrays.asList(user1, user2);
		
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		Role roleUser = new Role();
		
		osoba1.setRole(roleUser);
		osoba2.setRole(roleGuest);
		osoba3.setRole(roleAdmin);
		
		Map<Boolean, List<User>> result = UserService.partitionUserByUnderAndOver18(users);
		Map<Boolean, List<User>> test = new HashMap<Boolean, List<User>>();
		test.put(false, under18List );
		test.put(true, over18List);
		assertEquals(test,result);
	}

}
