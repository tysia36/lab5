package lab6.l6.lab6.src;


import java.util.Arrays;
import java.util.List;
import java.util.Map;

import lab6.l6.lab6.src.domain.*;
import lab6.l6.lab6.src.service.*;

	class Main {
	private static List<User> users;

	private static void init() {
		
		Person osoba1 = new Person();
		Person osoba2 = new Person();
		Person osoba3 = new Person();
		
		
		osoba1.setAge(25);
		osoba1.setName("Aniela");
		osoba1.setSurname("��cka");
		
		osoba2.setAge(15);
		osoba2.setName("Anna");
		osoba2.setSurname("Romowa");
		
		
		osoba3.setAge(57);
		osoba3.setName("Andrzej");
		osoba3.setSurname("Nowak");
		
		User user1 = new User("alacka", "P@ssw0rd123", osoba1);
		User user2 = new User("aromowa", "P@ssw0rd321", osoba2);
		User user3 = new User("anowak", "P@ssw0rd098", osoba3);
		
		
		users = Arrays.asList(user1, user2, user3);
		
		Role roleUser = new Role();
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		osoba1.setRole(roleUser);
		osoba2.setRole(roleGuest);
		osoba3.setRole(roleAdmin);
	
		
		Address address1 = new Address("Polska", "Krak�w", "Warszawska", 53);
		Address address2 = new Address("Poland", "Warszawa", "Krakowska",123);
		Address address3 = new Address("Poland", "Katowice", "Gda�ska", 98);
		
		osoba1.setAddresses(Arrays.asList(address1));
		osoba2.setAddresses(Arrays.asList(address1,address2,address3));
		osoba3.setAddresses(Arrays.asList(address1,address2));
		
		Permission permissionE = new Permission();
		Permission permissionW = new Permission();
		Permission permissionR = new Permission();
		
		permissionE.setName("execute");
		permissionW.setName("write");
		permissionR.setName("read");
		
		
		List<Permission> permmissionUser = Arrays.asList(permissionW,permissionR);
		List<Permission> permmissionGuest = Arrays.asList(permissionR);
		List<Permission> permmissionAdmin = Arrays.asList(permissionW,permissionR,permissionE);
		
		roleUser.setPermissions(permmissionUser);
		roleGuest.setPermissions(permmissionGuest);
		roleAdmin.setPermissions(permmissionAdmin);
		List<User> moreThan2Addresses = UserService.findUsersWhoHaveMoreThanOneAddress(users);
		System.out.println(moreThan2Addresses);
		
		Person oldest = UserService.findOldestPerson(users);
		System.out.println("Osoba Najstarsza to: " + oldest.getName() + " " 
							+ oldest.getSurname() + " lat " + oldest.getAge());
		User longest = UserService.findUserWithLongestUsername(users);
		System.out.println("Najdluzszy login ma: " + longest.getName());
		
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		System.out.println("Kto jest pelnoletni:" + result);
		
		List<String> nameStartingWithA = UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users);
		System.out.println("uprawnienia osob z imieniem na A: ");
		System.out.println(nameStartingWithA);

		System.out.println("Lista uprawnien na S: ");
		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
		
		Map<Role, List<User>> groupUsersByRole = UserService.groupUsersByRole(users);
		System.out.println(groupUsersByRole);
		
		Map<Boolean, List<User>> groupUsersBy18 = UserService.partitionUserByUnderAndOver18(users);
		System.out.println(groupUsersBy18);
	
	}
	


	public static void main(String[] args) {
		init();
	
	}
}
